#pragma once

#include <string>
#include <map>

#include <QtWidgets/QMainWindow>
#include "./ui_cmakehelp.h"

#include "qfile.h"
#include "qlist.h"
#include "qtextstream.h"
#include <functional>
#include "parameter_value.h"

#include "callback_monitor.h"

class ConfigTreeModel;

class CMakeHelp : public QMainWindow
{
    Q_OBJECT

	ValueComplex::PTR_T             m_entryNode;
	ValueComplex::PTR_T             m_projectNode;
	Value<std::string>::PTR_T       m_projectName;
	ValueSelection::PTR_T           m_cpuType;
	Value<std::string>::PTR_T       m_srcFile;
	Value<std::string>::PTR_T       m_dstFile;
	ValueComplex::PTR_T             m_makeInput;
	ValueComplex::PTR_T             m_settings;
	ValueComplex::PTR_T             m_includePathList;
	ValueComplex::PTR_T             m_cubeMxModules;
    Value<uint32_t>::PTR_T          m_testVarUint32;
    Value<std::string>::PTR_T       m_testVarStdStr;

    QString                         m_startupMod;
	std::string						m_mainModName;

    std::map<std::string, std::vector<std::string>>                             m_cModuleSrcFiles;
    std::map<std::string, std::function<void(std::string, QTextStream&)>>       m_parseCallbacks;

    CallbackMonitor                 m_connector;

public:
    CMakeHelp(QWidget *parent = Q_NULLPTR);
    ~CMakeHelp();

public slots:
    void onButtonPressed();
    void onReadMakefile();

    void onLoadProject();
    void onSaveProject();

private:
    Ui::CMakeHelpClass ui;

    bool addIncludePath(const QString& path);

    bool readCIncludes(QTextStream& in);
    bool readSources(QTextStream& in);
    void addSrcFile(std::string module, std::string fname);

    bool parseSetting(std::string first, QTextStream& src);

    int checkEntry(ValueComplex::PTR_T node, const std::string& entry);
    bool getEntry(ValueComplex::PTR_T node, const char* id, std::string& value);

    bool findMainModule(const ValueComplex::PTR_T& node, std::string& module);

	bool writePreamble(QTextStream& out);

    void writeTargetInclude(const std::string& modName, QTextStream& stream);
    bool writeModule(ValueComplex::PTR_T value, QTextStream& stream);

	bool writeLibraries(QTextStream& stream);
	bool writeOptLvl(QTextStream& stream);
	bool writeMainModule(QTextStream& stream);
	bool writeCubeMxCore(QTextStream& stream);
	bool writeDepencies(QTextStream& stream);

	bool writeIncludeDirectories(QTextStream& stream);

    bool writeCMakeFile();
    void addLogMsg(const QString& msg);
    void logMsg(const char* fmt, ...);
};
