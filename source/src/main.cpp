#include "cmakehelp.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CMakeHelp w;
    w.show();
    return a.exec();
}
