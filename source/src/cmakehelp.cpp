#include "qmessagebox.h"
#include "qfiledialog.h"

#include "cmakehelp.h"
#include "config_tree_model.h"
#include "config_tree_delegate.h"
#include "qdebug.h"

#include <stdarg.h>

#include "line_edit_binding.h"

#include <stdio.h>
#include <iostream>
#include <memory>

CMakeHelp::CMakeHelp(QWidget *parent)
    : QMainWindow(parent)
    , m_entryNode(ValueComplex::create("EntryNode"))
    , m_projectNode(ValueComplex::create(m_entryNode, "Project"))
    , m_projectName(Value<std::string>::create(m_projectNode, "ProjectName", "give_me_a_name"))
    , m_cpuType(ValueSelection::create(m_projectNode, "CPUType"))
    , m_srcFile(Value<std::string>::create(m_projectNode, "Makefile", "-"))
    , m_dstFile(Value<std::string>::create(m_projectNode, "CmakeFile", "-"))
    , m_makeInput(ValueComplex::create(m_projectNode, "Make"))
    , m_settings(ValueComplex::create(m_projectNode, "Setting"))
    , m_includePathList(ValueComplex::create(m_makeInput, "IncludePathList"))
    , m_cubeMxModules(ValueComplex::create(m_projectNode, "CubeMxModules"))
    , m_testVarUint32(Value<uint32_t>::create(m_projectName, "VarName", 200))
    , m_testVarStdStr(Value<std::string>::create(m_projectName, "StringVar", "Hotzenplotz"))
{
    ui.setupUi(this);

    m_connector.connect<uint32_t>(m_testVarUint32, [this](Value<uint32_t>::PTR_T val) 
    {
        std::cout << "Value changed !" << std::endl;
    });

    m_testVarUint32->set(130);


    m_includePathList->setProperty("Caption", "Include directorys");

    Value<std::string>::create(m_cpuType, "STM32F103", "103");
	Value<std::string>::create(m_cpuType, "STM32F403", "403");
	Value<std::string>::create(m_cpuType, "STM32F503", "503");
	Value<std::string>::create(m_cpuType, "STM32F603", "603");

    ui.m_projectTreeView->setEntryNode(m_entryNode);

    LineEditBinding::bind(ui.leMakefile, m_srcFile);
    LineEditBinding::bind(ui.leCmakeLists, m_dstFile);

    auto simpleAssign = [this](std::string first, QTextStream& src) 
    {
        QStringList list = QString(first.c_str()).split('=');

        std::string name = list[0].trimmed().toStdString();
        QString tmp = QString(first.c_str()).trimmed();
        tmp.remove(0, name.size());
        tmp = tmp.trimmed();
        tmp.remove(0, 1);
        tmp = tmp.trimmed();

        if (tmp.endsWith('\\')) 
        {
            tmp.remove(tmp.length() - 1, 1);
            // more than one line ..
            bool moreLine = true;
            do 
            {
                QString nextLine = src.readLine();
                nextLine = nextLine.trimmed();
                if (nextLine.endsWith('\\')) 
                {
                    nextLine.remove(nextLine.length() - 1, 1);
                }
                else 
                {
                    moreLine = false;
                }
                tmp += " " + nextLine;
            } while (moreLine);
        }

        tmp = tmp.trimmed();

		Value<std::string>::create(m_settings, name, tmp.toStdString());
    };

    m_parseCallbacks.emplace("CPU",             simpleAssign);
    m_parseCallbacks.emplace("FPU",             simpleAssign);
    m_parseCallbacks.emplace("FLOAT-ABI",       simpleAssign);
    m_parseCallbacks.emplace("MCU",             simpleAssign);
    m_parseCallbacks.emplace("TARGET",          simpleAssign);
    m_parseCallbacks.emplace("LIBS",            simpleAssign);
    m_parseCallbacks.emplace("LIBDIR",          simpleAssign);
    m_parseCallbacks.emplace("C_DEFS",          simpleAssign);
    m_parseCallbacks.emplace("LDFLAGS",         simpleAssign);
    m_parseCallbacks.emplace("ASM_SOURCES",     simpleAssign);
	m_parseCallbacks.emplace("LDSCRIPT",     	simpleAssign);
	m_parseCallbacks.emplace("C_SOURCES", [this](std::string first, QTextStream& src)
	{
		readSources(src);
	});
	m_parseCallbacks.emplace("C_INCLUDES", [this](std::string first, QTextStream& src)
	{
		readCIncludes(src);
	});
}

CMakeHelp::~CMakeHelp()
{
	m_entryNode.reset();
	m_projectNode.reset();
	m_projectName.reset();
	m_cpuType.reset();
	m_srcFile.reset();
	m_dstFile.reset();
	m_makeInput.reset();
	m_settings.reset();
	m_includePathList.reset();
	m_cubeMxModules.reset();
}

void CMakeHelp::logMsg(const char* fmt, ...)
{
    char prtBuff[512];
    va_list lst;

    va_start(lst, fmt);
    vsnprintf(prtBuff, sizeof(prtBuff), fmt, lst);
    va_end(lst);

    addLogMsg(prtBuff);
}

bool CMakeHelp::findMainModule(const ValueComplex::PTR_T& node, std::string& module)
{
	for(auto child : node->getChilds())
	{
		if(child->getType() == VALUE_TYPE::COMPLEX)
		{
			if(findMainModule(ValueComplex::cast(child), module)) 
			{
				// found !!
				return true;
			}
		}

		std::string name = child->getValueName();
		auto found = name.find("main.c");
		if(found != std::string::npos) 
		{
			found = name.find_first_of('/');
			if(found != std::string::npos)
			{
				// get the module name
				module = name.substr(0, found);
				return true;
			}
		}
	}
	return false;
}

void CMakeHelp::writeTargetInclude(const std::string& modName, QTextStream& stream)
{
	stream
		<< "target_include_directories("
		<< modName.c_str()
		<< " PRIVATE ${PROJECT_INCLUDE})\n";
}

bool CMakeHelp::writeModule(ValueComplex::PTR_T module, QTextStream& stream)
{
    std::string modName = module->getValueName();

    stream << "set ( SRC_" << modName.c_str() << "_FILES" << "\n";

    for(const ValueBase::PTR_T& item : module->getChilds())
    {
		if (item->getType() != VALUE_TYPE::COMPLEX)
		{
			stream << "    ${CMAKE_CURRENT_SOURCE_DIR}/";
			stream << item->getValueName().c_str();
			stream << "\n";
		}
    }

    stream << ")\n\n";

    return true;
}

bool CMakeHelp::writeDepencies(QTextStream& stream)
{
	stream << "\n\n";
	stream << "####### Library dependencies ########\n";
	stream << "target_link_libraries(${APP_NAME} ";

	for (const ValueBase::PTR_T& module : m_cubeMxModules->getChilds())
	{
		if (m_mainModName == module->getValueName())
		{
			continue;
		}
		stream << "mod_" << module->getValueName().c_str() << " ";
	}
	stream << ")\n";

	for (const ValueBase::PTR_T& module : m_cubeMxModules->getChilds())
	{
		const ValueBase::PTR_T& dependencies = module->getChild("Dependencies");
		if (dependencies)
		{
			std::string depLibs;

			for (const ValueBase::PTR_T& item : dependencies->getChilds())
			{
				Value<bool>::PTR_T boolItem = Value<bool>::cast(item);

				// target_link_libraries(mod_USB_DEVICE mod_Middlewares)

				if (boolItem->get())
				{
					if (depLibs.length())
					{
						depLibs += "  ";
					}
					depLibs += "mod_";
					depLibs += boolItem->getValueName();
				}
			}

			if(depLibs.length()) 
			{
				// we have one ore more dependencies ...

				stream << "target_link_libraries( mod_";
				stream << module->getValueName().c_str();
				stream << "  ";
				stream << depLibs.c_str();
				stream << " )\n";
			}
		}
	}

	return true;
}

bool CMakeHelp::writePreamble(QTextStream& stream)
{
	stream << "cmake_minimum_required(VERSION 3.0)\n";
	stream << "project(CubeCoreFramework C ASM)\n\n";

	stream << "SET(APP_NAME   ";
	stream << m_projectName->get().c_str();
	stream << ")\n";

	std::string cpu;

	if (getEntry(m_settings, "CPU", cpu))
	{
		stream << "SET(CPU        \"";
		stream << cpu.c_str();
		stream << "\")\n";
	}

	std::string fpu;

	if (getEntry(m_settings, "FPU", fpu))
	{
		stream << "SET(FPU        \"";
		stream << fpu.c_str();
		stream << "\")\n";
	}

	std::string floatAbi;

	if (getEntry(m_settings, "FLOAT-ABI", floatAbi))
	{
		stream << "SET(FLOAT-ABI  \"";
		stream << floatAbi.c_str();
		stream << "\")\n";
	}

	std::string mcu;

	if (getEntry(m_settings, "MCU", mcu))
	{

	}

	stream << "\n";

	std::string cDefs;

	if (getEntry(m_settings, "C_DEFS", cDefs))
	{
		QString tmp = QString(cDefs.c_str());
		QStringList list = tmp.split("-D");
		for (int n = 0; n < list.size(); n++)
		{
			if (0 == list[n].length())
			{
				continue;
			}
			stream << "add_definitions(-D ";
			stream << list[n];
			stream << ")\n";
		}
	}

	stream << "\n";

	std::string linkerScript;

	if (getEntry(m_settings, "LDSCRIPT", linkerScript))
	{
		stream
			<< "SET(APPCFG_LDSCRIPT ${CMAKE_CURRENT_SOURCE_DIR}/"
			<< linkerScript.c_str()
			<< ")\n";
	}

	stream
		//<< "SET(APPCFG_LDSCRIPT ${CMAKE_CURRENT_SOURCE_DIR}/STM32F411CEUx_FLASH.ld)\n"
		<< "SET(LIBDIR ${CMAKE_BINARY_DIR})\n"
		<< "SET(LDFLAGS \"--specs=nosys.specs --specs=nano.specs -T${APPCFG_LDSCRIPT} -L${LIBDIR} ${LIBS} -Wl,-Map=link.map,--cref -Wl,--gc-sections\")\n"
		<< "SET(CMAKE_EXE_LINKER_FLAGS \"${LDFLAGS}\")\n"
		<< "SET(CMAKE_C_FLAGS \"${CPU} ${FPU} ${FLOAT-ABI} -g -gdwarf-2 -fdata-sections -ffunction-sections\")\n"
		<< "SET(CMAKE_CXX_FLAGS \"${MCU} ${CPU} ${FPU} ${FLOAT-ABI} -g -gdwarf-2 -std=c++14 -fdata-sections -ffunction-sections\")\n";

	stream << "\n";

	return true;
}

bool CMakeHelp::writeIncludeDirectories(QTextStream& stream)
{
	stream << "SET( PROJECT_INCLUDE\n";

	for (const ValueBase::PTR_T& item : m_includePathList->getChilds())
	{
		stream
			<< "    "
			<< "${CMAKE_CURRENT_SOURCE_DIR}/"
			<< item->getValueName().c_str()
			<< "\n";
	}
	stream << ")\n";

	return true;
}

bool CMakeHelp::writeOptLvl(QTextStream& stream)
{
	for (const ValueBase::PTR_T& module : m_cubeMxModules->getChilds())
	{
		stream << "SET(OPTLVL_" << module->getValueName().c_str() << " \"-Os\" CACHE STRING";
		stream << " \"Optimization level " << module->getValueName().c_str() << "\")\n";
	}
	stream << "\n";
	return true;
}

bool CMakeHelp::writeMainModule(QTextStream& stream)
{
	if (m_mainModName.length()) 
	{
		ValueBase::PTR_T module = m_cubeMxModules->getChild(m_mainModName);

		if (module->getValueType() != VALUE_TYPE::COMPLEX)
		{
			return false;
		}

		std::string asmStartupFile;

		ValueBase::PTR_T asmSrc = m_settings->getChild("ASM_SOURCES");
		if (asmSrc)
		{
			if (asmSrc->getValueType() == VALUE_TYPE::STRING)
			{
				asmStartupFile = Value<std::string>::cast(asmSrc)->get();
			}
		}

		ValueComplex::PTR_T items = ValueComplex::cast(module);

		stream << "set ( SRC_" << module->getValueName().c_str() << "_FILES" << "\n";

		for (const ValueBase::PTR_T& item : items->getChilds())
		{
			if (item->getValueType() != VALUE_TYPE::COMPLEX)
			{
				stream << "    ${CMAKE_CURRENT_SOURCE_DIR}/";
				stream << item->getValueName().c_str();
				stream << "\n";
			}
		}
		if (asmStartupFile.length())
		{
			stream << "    ";
			stream << asmStartupFile.c_str();
			stream << "\n";
		}

		stream << ")\n\n";
		stream << "add_executable(${APP_NAME} ${SRC_";
		stream << module->getValueName().c_str();
		stream << "_FILES}) \n\n";

		stream << "target_compile_options(${APP_NAME} PRIVATE ${CPU} ${THUMB} ${FPU} ${FLOAT_ABI} ${OPTLVL_";
		stream << m_mainModName.c_str();
		stream << "})\n\n";
		return true;
	}
	return false;
}

bool CMakeHelp::writeLibraries(QTextStream& stream)
{
	for (ValueBase::PTR_T module : m_cubeMxModules->getChilds())
	{

		if (module->getValueName() == m_mainModName)
		{
			// skip main module
			continue;
		}

		stream << "\n\n\n";

		stream << "############################################### ";
		stream << module->getValueName().c_str();
		stream << " ###############################################";

		stream << "\n\n";

		writeModule(ValueComplex::cast(module), stream);

		stream << "\n";
		stream << "add_library(mod_";
		stream << module->getValueName().c_str();
		stream << " STATIC ${SRC_";
		stream << module->getValueName().c_str();
		stream << "_FILES})\n";

		std::string libName = "mod_" + module->getValueName();

		writeTargetInclude(libName, stream);

		stream << "\n";
		stream << "target_compile_options(";
		stream << libName.c_str();
		stream << " PRIVATE ${CPU} ${THUMB} ${FPU} ${FLOAT_ABI} ${OPTLVL_";
		stream << module->getValueName().c_str();
		stream << "}) \n";
	}

	return true;
}

bool CMakeHelp::writeCubeMxCore(QTextStream& stream)
{

	for (ValueBase::PTR_T module : m_cubeMxModules->getChilds())
	{
		stream << "\n\n\n";

		stream << "############################################### ";
		stream << module->getValueName().c_str();
		stream << " ###############################################";

		stream << "\n\n";

		writeModule(ValueComplex::cast(module), stream);

		stream << "\n";
		stream << "add_library(mod_";
		stream << module->getValueName().c_str();
		stream << " STATIC ${SRC_";
		stream << module->getValueName().c_str();
		stream << "_FILES})\n\n\n";

		std::string libName = "mod_" + module->getValueName();

		writeTargetInclude(libName, stream);

		stream << "\n";
		stream << "target_compile_options(";
		stream << libName.c_str();
		stream << " PRIVATE ${CPU} ${THUMB} ${FPU} ${FLOAT_ABI} ${OPTLVL_ALL}) \n";
	}

	stream << "\n";

	return true;
}

bool CMakeHelp::writeCMakeFile()
{
	QString fileName = m_dstFile->get().c_str();

	if (fileName.length() == 0)
	{
		return false;
	}

	QFile file(fileName);

	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		return false;
	}

	QTextStream stream(&file);

	writePreamble(stream);
	writeOptLvl(stream);

	writeIncludeDirectories(stream);

	writeMainModule(stream);
	writeTargetInclude("${APP_NAME}", stream);
	writeLibraries(stream);

	writeDepencies(stream);
	return true;
}

bool CMakeHelp::getEntry(ValueComplex::PTR_T node, const char* id, std::string& value)
{
    for( ValueBase::PTR_T entry : node->getChilds()) 
    {
		Value<std::string>::PTR_T p = Value<std::string>::cast(entry);

		if (0 == p->getValueName().compare(id))
		{
			value = p->get();
			return true;
		}
    }
    return false;
}

int CMakeHelp::checkEntry(ValueComplex::PTR_T node, const std::string& entry)
{
    int idx=0;

    for(const ValueBase::PTR_T& item : node->getChilds()) 
    {
        if(item->getValueName() == entry) 
        {
            return idx;
        }
        idx++;
    }
    return -1;
}

void CMakeHelp::addSrcFile(std::string module, std::string fname)
{
	auto pos = fname.find("main.c", 0);
	if (pos != std::string::npos)
	{
		m_mainModName = module;
	}

    auto ref = m_cModuleSrcFiles.find(module);

    if (ref == m_cModuleSrcFiles.end()) 
    {
        // new module found
        std::vector<std::string> newList;
        newList.emplace_back(fname);
        m_cModuleSrcFiles.emplace(module, newList);
    }
    else 
    {
        // append file to module
        ref->second.emplace_back(fname);
    }
}

bool CMakeHelp::parseSetting(std::string first, QTextStream& src)
{
    QStringList list = QString(first.c_str()).split('=');

    if (list.count() < 2) 
    {
        return false;
    }

    auto p = m_parseCallbacks[list[0].trimmed().toStdString()];

    if (p != nullptr) 
    {
        p(first, src);
        return true;
    }

    return false;
}

bool CMakeHelp::readSources(QTextStream& in)
{
    QString         module;
    QStringList     sList;

    addLogMsg("read source ....");
    bool done = false;
    while (!in.atEnd())
    {
        QString line = in.readLine();

        sList = line.split('/');
       
        if (line.endsWith('\\')) 
        {
            line.remove('\\');
            line = line.trimmed();
        }
        else 
        {
            done = true;
        }

        addSrcFile(sList[0].toStdString(), line.toStdString());

        if (done) 
        {
            //this was the last line ...
            return true;
        }
    }

    return false;
}

bool CMakeHelp::addIncludePath(const QString& path)
{
    if (-1 == checkEntry(m_includePathList, path.toStdString())) 
    {
        Value<bool>::create(m_includePathList, path.toStdString(), true);
    }

    return true;
}

bool CMakeHelp::readCIncludes(QTextStream& in)
{
    addLogMsg("read C include ....");
    while (!in.atEnd())
    {
        QString line = in.readLine();

        if (line.endsWith('\\')) 
        {
            QStringList sList = line.split(' ');
            if (sList.count() == 2) 
            {
                QString incpath = sList[0];
                if (incpath.startsWith("-I")) 
                {
                    incpath.remove(0, 2);
                }
                addIncludePath(incpath);
            }
        }
        else {
            if (line.length() != 0) 
            {
                QString incpath = line;
                if (incpath.startsWith("-I")) 
                {
                    incpath.remove(0, 2);
                }
                addIncludePath(incpath);
                return true;
            }
        }
    }

    return false;
}

void CMakeHelp::onReadMakefile()
{
    QString fileName = m_srcFile->get().c_str();

    ui.pteMessages->clear();

    if (fileName.length() == 0) 
    {
        return;
    }

#ifdef __UNUSED__
    why that ??
    m_includePathList->removeChilds();
    m_subProjects->removeChilds();
    m_cModuleSrcFiles.clear();
#endif

    QFile inputFile(fileName);
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            line = line.trimmed();

            if (parseSetting(line.toStdString(), in)) 
            {
                continue;
            }
        }
        inputFile.close();
    }

    // create entries in tree view 
    for (auto ref : m_cModuleSrcFiles) 
    {
        ValueComplex::PTR_T module = ValueComplex::create(m_cubeMxModules, ref.first.c_str());

        for (int n = 0; n < ref.second.size(); n++) 
        {
            Value<bool>::create(module, ref.second[n].c_str(), true);

            // check for main entry
            if (std::string::npos != ref.second[n].find("main.c")) 
            {
                module->setProperty<bool>("MainModule", true);
            }
        }

		ValueComplex::PTR_T dependencies = ValueComplex::create(module, "Dependencies");
		for (auto item : m_cModuleSrcFiles)
		{
			if (ref.first != item.first)
			{
				Value<bool>::create(dependencies, item.first, false);
			}
		}
    }

    m_cModuleSrcFiles.clear();
}

void CMakeHelp::addLogMsg(const QString& msg)
{
    ui.pteMessages->appendPlainText(msg);
}

void CMakeHelp::onButtonPressed()
{
    QObject* sigSrc = sender();

    if (sigSrc == ui.pbClear) 
    {
        ui.pteMessages->clear();
    }
    else if (sigSrc == ui.pbReadMakefile) 
    {
        onReadMakefile();
    }
    else if (sigSrc == ui.pbWriteCmake) 
    {
        writeCMakeFile();
    }
    else if (sigSrc == ui.pbClearSubProjects) 
    {
        m_cubeMxModules->removeChilds();
        m_cModuleSrcFiles.clear();
    }
}

void CMakeHelp::onLoadProject()
{
    QString fName = QFileDialog::getOpenFileName(this, "Load project file", "*");
    std::string msg;

    ValueBase::loadFromFile(msg, fName.toStdString(), m_entryNode.operator->());

	findMainModule(m_entryNode, m_mainModName);
}

void CMakeHelp::onSaveProject()
{
    QString fName = QFileDialog::getSaveFileName(this, "Save project file", "*");
    std::string msg;

    ValueBase::saveToFile(msg, fName.toStdString(), m_entryNode.operator->());
}
