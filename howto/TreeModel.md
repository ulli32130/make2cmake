# Some remarks about TreeItemModel

## ConfigTreeItem

- m_model

    pointer to class _ConfigTreeModel_

- m_parentItem

    pointer to class _ConfigTreeItem_, parent, maybe NULL

- m_childItems

    vector of class _ConfigTreeItem_, containing childs, maybe 0

## Remove an item

    void MainWindow::removeRow()
    {
        const QModelIndex index = view->selectionModel->currentIndex();
        QAbstractItemModel *model = view->model();
        if (model->removeRow(index.row(), index.parent()))
            updateActions();
    } 


    bool TreeModel::removeRows(int position, int rows, const QModelIndex &parent)
    {
        TreeItem *parentItem = getItem(parent);
        if (!parentItem)
            return false;

        beginRemoveRows(parent, position, position + rows - 1);
        const bool success = parentItem->removeChildren(position, rows);
        endRemoveRows();

        return success;
    }